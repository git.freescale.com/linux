/*
 * Copyright 2012 Freescale Semiconductor, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * CPU Frequency Scaling for Freescale SoCs with PLL switching capability.
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/cpufreq.h>
#include <linux/init.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/io.h>

#define CORE_OFFSET				0x0
#define CORE_STEP				0x20
#define PLL_OFFSET				0x800
#define PLL_STEP				0x20
#define PLL_KILL				(1 << 31)

#define	CLKSEL_SHIFT			27
#define MAX_FREQ_PER_PLL		4

#define READ_PLL_REG(base, pll)	\
	in_be32((base) + PLL_OFFSET + (pll) * PLL_STEP)
#define READ_CORE_REG(base, cpu)	\
	in_be32((base) + CORE_OFFSET + (cpu) * CORE_STEP)
#define WRITE_CORE_REG(base, cpu, val)	\
	out_be32((base) + CORE_OFFSET + (cpu) * CORE_STEP, (val))

/**
 * struct cpufreq_data - cpufreq driver data
 * @base: the base address of clocking module
 * @sysclk: input clock frequency in khz
 * @cpufreq_lock: the mutex lock
 * @soc: platform specific data
 */
struct cpufreq_data {
	void __iomem *base;
	u32	sysclk;
	struct mutex cpufreq_lock;
	const struct cpufreq_soc *soc;
};

/**
 * struct cpufreq_soc - board-specific data
 * @compatible: compatible string
 * @freq_bitmap: frequency group bitmap, each available
 *		frequency maps to 1 bit. The format is:
 *       ...   pll1  pll0/8 pll0/4 pll0/2 pll0
 *              1      1      1      1      1
 *		Exa. the 0x33 means there are 4 frequencies
 *		in this group: pll1/2, pll1, pll0/2, pll0
 * @mask: group number mask to extract the group from CPU id
 * @cnt: clock source number in each group
 * @pll: pll number on soc
 * @cpu_per_cluster: cpu number per cluster
 * @sel_adjust: adjust the PLL base number, assist calculation
 */
struct cpufreq_soc {
	char *compatible;
	u32 freq_bitmap[2];
	u32 mask;
	unsigned int count[2];
	unsigned int pll;
	unsigned int cpu_per_cluster;
	unsigned int sel_adjust;
};

/**
 * struct clock_group - frequency table list
 * @num: the frequency table index
 * @table: the frequency table point
 * @entry: table list
 */
struct clock_group {
	unsigned int num;
	struct cpufreq_frequency_table *table;
	struct list_head entry;
};

/**
 * struct cpumask_list- cpumask_var_t list
 * @cpumask: the cpumask variable
 * @entry: the list entry
 */
struct cpumask_list {
	cpumask_var_t cpumask;
	struct list_head entry;
};

/* SoC specific data */
const struct cpufreq_soc socs[] = {
	{
		.compatible = "fsl,p5020-clockgen",
		.freq_bitmap = {0x77, 0},
		.pll = 2,
		.count = {6, 0},
		.mask = ~1,
		.cpu_per_cluster = 1,
		.sel_adjust = 0,
	},
	{
		.compatible = "fsl,p5040-clockgen",
		.freq_bitmap = {0x33, 0},
		.pll = 2,
		.count = {4, 0},
		.mask = ~3,
		.cpu_per_cluster = 1,
		.sel_adjust = 0,
	},
	{
		.compatible = "fsl,p2041-clockgen",
		.freq_bitmap = {0x77, 0},
		.pll = 2,
		.count = {6, 0},
		.mask = ~3,
		.cpu_per_cluster = 1,
		.sel_adjust = 0,
	},
	{
		.compatible = "fsl,p4080-clockgen",
		.freq_bitmap = {0x33, 0x3300},
		.pll = 4,
		.count = {4, 4},
		.mask = ~3,
		.cpu_per_cluster = 1,
		.sel_adjust = 0,
	},
	{
		.compatible = "fsl,p3041-clockgen",
		.freq_bitmap = {0x77, 0},
		.pll = 2,
		.count = {6, 0},
		.mask = ~3,
		.cpu_per_cluster = 1,
		.sel_adjust = 0,
	},
	{
		.compatible = "fsl,b4860-clockgen",
		.freq_bitmap = {0x77, 0},
		.pll = 2,
		.count = {6, 0},
		.mask = ~7,
		.cpu_per_cluster = 8,
		.sel_adjust = 0,
	},
	{
		.compatible = "fsl,t4240-clockgen",
		.freq_bitmap = {0x777, 0x77000},
		.pll = 5,
		.count = {9, 6},
		.mask = ~0xf,
		.cpu_per_cluster = 8,
		.sel_adjust = 3,
	},
};

static DEFINE_PER_CPU(struct cpufreq_frequency_table *, cpu_clk);
static LIST_HEAD(group_list);
static LIST_HEAD(cpumask_list);
static struct cpufreq_data freq_data;

static unsigned int corenet_cpufreq_get_speed(unsigned int cpu)
{
	u32 clksel;
	unsigned int i, freq;
	struct cpufreq_frequency_table *table;
	unsigned int hwcpu = get_hard_smp_processor_id(cpu);
	hwcpu /= freq_data.soc->cpu_per_cluster;

	table = per_cpu(cpu_clk, cpu);
	if (!table)
		return 0;

	clksel = READ_CORE_REG(freq_data.base, hwcpu);
	clksel = (clksel >> CLKSEL_SHIFT) & 0xf;

	/* look up the index related to clksel in table */
	for (i = 0; table[i].frequency != CPUFREQ_TABLE_END; i++) {
		freq = table[i].frequency;

		if (freq == CPUFREQ_ENTRY_INVALID)
			continue;
		if (table[i].index == clksel)
			return table[i].frequency;
	}

	return 0;
}

static int corenet_cpufreq_cpu_init(struct cpufreq_policy *policy)
{
	struct clock_group *clk;
	struct cpumask_list *cpu_list, *tmp;
	unsigned int cpu = policy->cpu;
	unsigned int grp;
	int i;

	cpu_list = kzalloc(sizeof(struct cpumask_list), GFP_KERNEL);
	if (!cpu_list)
		return -ENOMEM;

	INIT_LIST_HEAD(&cpu_list->entry);
	list_add(&cpu_list->entry, &cpumask_list);

	if (!zalloc_cpumask_var(&cpu_list->cpumask, GFP_KERNEL))
		goto err_mask;

	for (i = cpu; i < freq_data.soc->cpu_per_cluster + cpu; i++)
		cpumask_set_cpu(i, cpu_list->cpumask);

	/* get the cpu clock group */
	grp = get_hard_smp_processor_id(cpu);
	grp = !!(grp & (freq_data.soc->mask));

	/* look up the frequency table according to the group */
	list_for_each_entry(clk, &group_list, entry) {
		if (grp == clk->num)
			break;
	}
	if (!clk)
		goto err_mask;

	/* set the frequency table */
	per_cpu(cpu_clk, cpu) = clk->table;
	cpufreq_frequency_table_get_attr(clk->table, cpu);

	/* FIXME: what's the actual transition time? in ns */
	policy->cpuinfo.transition_latency = 200000;

	policy->cur = corenet_cpufreq_get_speed(policy->cpu);
	cpumask_copy(policy->cpus, cpu_list->cpumask);

	/* set the min and max frequency properly */
	i = cpufreq_frequency_table_cpuinfo(policy, clk->table);
	if (i)
		goto err_mask;

	return 0;

err_mask:
	list_for_each_entry_safe(cpu_list, tmp, &cpumask_list, entry) {
		free_cpumask_var(cpu_list->cpumask);
		list_del(&cpu_list->entry);
		kfree(cpu_list);
	}

	return -ENODEV;
}

static int __devexit corenet_cpufreq_cpu_exit(struct cpufreq_policy *policy)
{
	cpufreq_frequency_table_put_attr(policy->cpu);

	return 0;
}

static int corenet_cpufreq_verify(struct cpufreq_policy *policy)
{
	struct cpufreq_frequency_table *table;

	table = per_cpu(cpu_clk, policy->cpu);
	if (!table)
		return -EINVAL;

	return cpufreq_frequency_table_verify(policy, table);
}

static int corenet_cpufreq_target(struct cpufreq_policy *policy,
		unsigned int target_freq, unsigned int relation)
{
	struct cpufreq_freqs freqs;
	unsigned int i, new;
	struct cpufreq_frequency_table *freq_table;
	unsigned int hwcpu = get_hard_smp_processor_id(policy->cpu);
	hwcpu /= freq_data.soc->cpu_per_cluster;

	freq_table = per_cpu(cpu_clk, policy->cpu);
	if (!freq_table)
		return -EINVAL;

	cpufreq_frequency_table_target(policy, freq_table,
			target_freq, relation, &new);

	if (policy->cur == freq_table[new].frequency)
		return 0;

	freqs.old = policy->cur;
	freqs.new = freq_table[new].frequency;
	freqs.cpu = policy->cpu;

	mutex_lock(&freq_data.cpufreq_lock);
	cpufreq_notify_transition(&freqs, CPUFREQ_PRECHANGE);

	i = (freq_table[new].index & 0xf) << CLKSEL_SHIFT;
	WRITE_CORE_REG(freq_data.base, hwcpu, i);
	pr_info("cpufreq: setting %s%d frequency from %d KHz to %d kHz\n",
		(freq_data.soc->cpu_per_cluster > 1) ? "cluster" : "core",
		 policy->cpu / freq_data.soc->cpu_per_cluster,
		 freqs.old,
		 freq_table[new].frequency);

	cpufreq_notify_transition(&freqs, CPUFREQ_POSTCHANGE);
	mutex_unlock(&freq_data.cpufreq_lock);

	return 0;
}

static struct freq_attr *corenet_cpu_clks_attr[] = {
	&cpufreq_freq_attr_scaling_available_freqs,
	NULL,
};

static struct cpufreq_driver ppc_corenet_cpufreq_driver = {
	.name		= "fsl_cpufreq",
	.owner		= THIS_MODULE,
	.flags		= CPUFREQ_CONST_LOOPS,
	.init		= corenet_cpufreq_cpu_init,
	.exit		= __devexit_p(corenet_cpufreq_cpu_exit),
	.verify		= corenet_cpufreq_verify,
	.target		= corenet_cpufreq_target,
	.get		= corenet_cpufreq_get_speed,
	.attr		= corenet_cpu_clks_attr,
};

/* reduce the duplicated frequency in frequency table */
static int freq_table_redup(struct cpufreq_frequency_table *freq_table,
		int cur)
{
	unsigned int i, freq = freq_table[cur].frequency;

	for (i = 0; i < cur; i++) {
		if (freq == freq_table[i].frequency)
			return 1;
	}

	return 0;
}

/* initialize the frequency table */
static int freq_table_init(int idx)
{
	unsigned int i, j, len, adj = 0;
	unsigned long pll_freq;
	u32 pll_mult, grp;
	struct clock_group *clk, *tmp;
	struct clock_group *clock_group;

	clock_group = kzalloc(sizeof(struct clock_group), GFP_KERNEL);
	if (!clock_group)
		goto err_clk;

	clock_group->num = idx;
	INIT_LIST_HEAD(&clock_group->entry);
	list_add(&clock_group->entry, &group_list);
	grp = freq_data.soc->freq_bitmap[idx];

	/*
	 * should alloc (frequency count) + 1 records,
	 * for last record is table END mark.
	 */
	clock_group->table = kcalloc(freq_data.soc->count[idx] + 1,
			sizeof(struct cpufreq_frequency_table), GFP_KERNEL);
	if (!clock_group->table)
		goto err_clk;

	/* set the clock select adjust */
	if (idx)
		adj = freq_data.soc->sel_adjust;
	len = 0;
	for (i = 0; i < freq_data.soc->pll; i++) {
		pll_mult = READ_PLL_REG(freq_data.base, i);

		/* make sure this PLL is not disabled */
		if (pll_mult & PLL_KILL)
			continue;

		/* caculate the pll frequency */
		pll_freq = ((pll_mult >> 1) & 0x3F) * freq_data.sysclk;

		for (j = 0; j < MAX_FREQ_PER_PLL; j++) {
			/*
			 * parse the frequency in this group.
			 * see the comments in struct socs
			 */
			if (((grp >> ((i << 2) + j))) & 0x1) {
				/* set the index to core's CLKSEL */
				clock_group->table[len].index =
					((i - adj) << 2) + j;
				clock_group->table[len].frequency =
					pll_freq / (1 << j);
				/* reduce the duplicated frequency */
				if (!freq_table_redup(clock_group->table, len))
					len++;
			}
		}
	}
	clock_group->table[len].index = -1;
	clock_group->table[len].frequency = CPUFREQ_TABLE_END;

	return 0;

err_clk:
	list_for_each_entry_safe(clk, tmp, &group_list, entry) {
		kfree(clk->table);
		list_del(&clk->entry);
		kfree(clk);
	}

	return -ENOMEM;
}

static int __init ppc_corenet_cpufreq_init(void)
{
	struct device_node *np;
	int i, ret = 0;
	const __be32 *valp;

	/* look for the supported board */
	for (i = 0; i < ARRAY_SIZE(socs); i++) {
		np = of_find_compatible_node(NULL, NULL,
				socs[i].compatible);
		if (np)
			break;
	}

	if (!np) {
		pr_debug("fsl_cpufreq: unsupported platform: %s\n",
				socs[i].compatible);
		return -ENODEV;
	}

	freq_data.soc = &socs[i];

	/* get the sysclk frequency */
	valp = of_get_property(np, "clock-frequency", NULL);
	if (!valp) {
		pr_err("fsl_cpufreq: could not get the clock frequency\n");
		goto err_dev;
	}

	/* convert the unit to Khz */
	freq_data.sysclk = be32_to_cpup(valp) / 1000;

	freq_data.base = of_iomap(np, 0);
	if (!freq_data.base) {
		pr_err("fsl_cpufreq: could not map the register\n");
		goto err_dev;
	}

	/*
	 * initialize the freq table for each group.
	 * we have at most 2 groups currently.
	 */
	for (i = 0; i < 2; i++) {
		if (freq_data.soc->freq_bitmap[i])
			if (freq_table_init(i)) {
				pr_err("fsl_cpufreq: initiate the frequency table error\n");
				goto err_map;
			}
	}

	mutex_init(&freq_data.cpufreq_lock);

	ret = cpufreq_register_driver(&ppc_corenet_cpufreq_driver);
	if (ret)
		goto err_map;

	of_node_put(np);
	pr_info("fsl_cpufreq: Freescale CPU frequency scaling driver\n");

	return ret;

err_map:
	iounmap(freq_data.base);

err_dev:
	of_node_put(np);

	return ret;
}

static void __exit ppc_corenet_cpufreq_exit(void)
{
	struct clock_group *clk, *tmp;
	struct cpumask_list *cpu_list, *tmp1;

	list_for_each_entry_safe(clk, tmp, &group_list, entry) {
		kfree(clk->table);
		list_del(&clk->entry);
		kfree(clk);
	}

	list_for_each_entry_safe(cpu_list, tmp1, &cpumask_list, entry) {
		free_cpumask_var(cpu_list->cpumask);
		list_del(&cpu_list->entry);
		kfree(cpu_list);
	}

	iounmap(freq_data.base);
	cpufreq_unregister_driver(&ppc_corenet_cpufreq_driver);
}

MODULE_LICENSE("GPL V2");
MODULE_AUTHOR("Tang Yuantian <Yuantian.Tang@freescale.com>");
MODULE_DESCRIPTION("cpufreq driver for Freescale e500mc series SoCs");

module_init(ppc_corenet_cpufreq_init);
module_exit(ppc_corenet_cpufreq_exit);
