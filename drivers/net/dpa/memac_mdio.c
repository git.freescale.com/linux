 /*
 * This file is licensed under the terms of the GNU
 * General Public License version 2.
 * This program is licensed "as is" without any warranty of any kind,
 * whether express or implied.
 *
 * QorIQ MEMAC MDIO Controller
 *
 * Authors:	Sandeep Singh <sandeep@freescale.com>
 *		Andy Fleming <afleming@freescale.com>
 *		Roy Zang <tie-fei.zang@freescale.com>
 *
 * Based on memac mdio code from uboot: drivers/net/fm/memac_phy.c
 */

#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/unistd.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mii.h>
#include <linux/phy.h>
#include <linux/mdio.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/of_mdio.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <asm/irq.h>

struct memac_mdio_controller {
	u32	res0[0xc];
	u32	mdio_stat;	/* MDIO configuration and status */
	u32	mdio_ctl;	/* MDIO control */
	u32	mdio_data;	/* MDIO data */
	u32	mdio_addr;	/* MDIO address */
} __packed;

#define MDIO_STAT_CLKDIV(x)	(((x>>1) & 0xff) << 8)
#define MDIO_STAT_BSY		(1 << 0)
#define MDIO_STAT_RD_ER		(1 << 1)
#define MDIO_CTL_DEV_ADDR(x)	(x & 0x1f)
#define MDIO_CTL_PORT_ADDR(x)	((x & 0x1f) << 5)
#define MDIO_CTL_PRE_DIS	(1 << 10)
#define MDIO_CTL_SCAN_EN	(1 << 11)
#define MDIO_CTL_POST_INC	(1 << 14)
#define MDIO_CTL_READ		(1 << 15)
#define MDIO_STAT_ENC		(1 << 6)
#define MDIO_STAT_HOLD_15_CLK	(7 << 2)

#define MDIO_DATA(x)		(x & 0xffff)
#define MDIO_DATA_BSY		(1 << 31)

/* Number of microseconds to wait for an MII register to respond */
#define MII_TIMEOUT	1000

int memac_mdio_write(struct mii_bus *bus, int port_addr,
			int dev_addr, int regnum, u16 value)
{
	struct memac_mdio_controller __iomem *regs = bus->priv;
	u32 mdio_ctl, mdio_stat;
	int status;

	mdio_stat = in_be32(&regs->mdio_stat);
	if (bus->is_c45) {
		if (dev_addr == MDIO_DEVAD_NONE)
			return 0xffff;
		mdio_stat = mdio_stat | MDIO_STAT_ENC | MDIO_STAT_HOLD_15_CLK;
	} else {
		/* Clause 22 */
		dev_addr = regnum & 0x1f;
		mdio_stat = mdio_stat & ~MDIO_STAT_ENC;
	}

	out_be32(&regs->mdio_stat, mdio_stat);

	/* Wait till the bus is free */
	status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
			& MDIO_STAT_BSY), MII_TIMEOUT, 0);
	if (!status) {
		dev_err(&bus->dev, "Timeout waiting for MII bus\n");
		return -ETIMEDOUT;
	}

	/* Set the port and dev addr */
	mdio_ctl = MDIO_CTL_PORT_ADDR(port_addr) | MDIO_CTL_DEV_ADDR(dev_addr);
	out_be32(&regs->mdio_ctl, mdio_ctl);

	/* Set the register address */
	if (bus->is_c45) {
		status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
				& MDIO_STAT_BSY), MII_TIMEOUT, 0);
		if (!status) {
			dev_err(&bus->dev, "Timeout waiting for MII bus\n");
			return -ETIMEDOUT;
		}
		out_be32(&regs->mdio_addr, regnum & 0xffff);
	}

	/* Wait till the bus is free */
	status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
			& MDIO_STAT_BSY), MII_TIMEOUT, 0);
	if (!status) {
		dev_err(&bus->dev, "Timeout waiting for MII bus\n");
		return -ETIMEDOUT;
	}

	/* Write the value to the register */
	out_be32(&regs->mdio_data, MDIO_DATA(value));

	/* Wait till the MDIO write is complete */
	status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
			& MDIO_DATA_BSY), MII_TIMEOUT, 0);
	if (!status)
			return -ETIMEDOUT;

	return 0;
}


int memac_mdio_read(struct mii_bus *bus, int port_addr, int dev_addr,
			int regnum)
{
	struct memac_mdio_controller __iomem *regs = bus->priv;
	u32 mdio_ctl, mdio_stat;
	int status;

	mdio_stat = in_be32(&regs->mdio_stat);
	if (bus->is_c45) {
		if (dev_addr == MDIO_DEVAD_NONE)
			return 0xffff;
		mdio_stat = mdio_stat | MDIO_STAT_ENC | MDIO_STAT_HOLD_15_CLK;
	} else {
		/* Clause 22 */
		dev_addr = regnum & 0x1f;
		mdio_stat = mdio_stat & ~MDIO_STAT_ENC;
	}

	out_be32(&regs->mdio_stat, mdio_stat);

	/* Wait till the bus is free */
	status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
			& MDIO_STAT_BSY), MII_TIMEOUT, 0);
	if (!status)
		return -ETIMEDOUT;

	/* Set the Port and Device Addrs */
	mdio_ctl = MDIO_CTL_PORT_ADDR(port_addr) | MDIO_CTL_DEV_ADDR(dev_addr);
	out_be32(&regs->mdio_ctl, mdio_ctl);

	/* Set the register address */
	if (bus->is_c45) {
		status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
				& MDIO_STAT_BSY), MII_TIMEOUT, 0);
		if (!status) {
			dev_err(&bus->dev, "Timeout waiting for MII bus\n");
			return -ETIMEDOUT;
		}
		out_be32(&regs->mdio_addr, regnum & 0xffff);
	}

	/* Wait till the bus is free */
	status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
			& MDIO_STAT_BSY), MII_TIMEOUT, 0);
	if (!status) {
		dev_err(&bus->dev, "Timeout waiting for MII bus\n");
		return -ETIMEDOUT;
	}

	/* Initiate the read */
	mdio_ctl |= MDIO_CTL_READ;
	out_be32(&regs->mdio_ctl, mdio_ctl);

	/* Wait till the MDIO write is complete */
	status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
			& MDIO_STAT_BSY), MII_TIMEOUT, 0);
	if (!status) {
		dev_err(&bus->dev, "Timeout waiting for MII bus\n");
		return -ETIMEDOUT;
	}

	/* Return all Fs if nothing was there */
	if (in_be32(&regs->mdio_stat) & MDIO_STAT_RD_ER)
		return 0xffff;

	return in_be32(&regs->mdio_data) & 0xffff;
}


/* Reset the MIIM registers, and wait for the bus to free */
static int memac_mdio_reset(struct mii_bus *bus)
{
	struct memac_mdio_controller __iomem *regs = bus->priv;
	int status;

	clrbits32(&regs->mdio_stat, MDIO_STAT_ENC);

	/* Wait till the bus is free */
	status = spin_event_timeout(!(in_be32(&regs->mdio_stat)
			& MDIO_STAT_BSY), MII_TIMEOUT, 0);

	if (!status) {
		dev_err(&bus->dev, "Timeout waiting for MII bus\n");
		return -EBUSY;
	}

	return 0;
}


static int memac_mdio_probe(struct platform_device *ofdev)
{
	struct memac_mdio_controller __iomem *regs;
	struct device_node *np = ofdev->dev.of_node;
	struct mii_bus *new_bus;
	u64 addr, size;
	int err = 0;

	new_bus = mdiobus_alloc();
	if (NULL == new_bus)
		return -ENOMEM;

	new_bus->name = "Freescale MEMAC MDIO Bus";
	new_bus->read = &memac_mdio_read;
	new_bus->write = &memac_mdio_write;
	new_bus->reset = &memac_mdio_reset;

	/* Set the PHY base address */
	addr = of_translate_address(np, of_get_address(np, 0, &size, NULL));
	regs = ioremap(addr, size);

	if (NULL == regs) {
		err = -ENOMEM;
		goto err_ioremap;
	}

	new_bus->priv = (void *)regs;

	new_bus->irq = kcalloc(PHY_MAX_ADDR, sizeof(int), GFP_KERNEL);

	if (NULL == new_bus->irq) {
		err = -ENOMEM;
		goto err_irq_alloc;
	}

	new_bus->parent = &ofdev->dev;
	dev_set_drvdata(&ofdev->dev, new_bus);

	sprintf(new_bus->id, "%s@%llx", np->name, (unsigned long long)addr);

	err = of_mdiobus_register(new_bus, np);

	if (err) {
		dev_err(&ofdev->dev, "%s: Cannot register as MDIO bus\n",
				new_bus->name);
		goto err_registration;
	}

	return 0;

err_registration:
	kfree(new_bus->irq);
err_irq_alloc:
	iounmap(regs);
err_ioremap:
	return err;
}


static int memac_mdio_remove(struct platform_device *ofdev)
{
	struct device *device = &ofdev->dev;
	struct mii_bus *bus = dev_get_drvdata(device);

	mdiobus_unregister(bus);

	dev_set_drvdata(device, NULL);

	iounmap((void __iomem *)bus->priv);
	bus->priv = NULL;
	mdiobus_free(bus);

	return 0;
}

static struct of_device_id memac_mdio_match[] = {
	{
		.compatible = "fsl,fman-memac-mdio",
	},
	{},
};

static struct platform_driver memac_mdio_driver = {
	.driver = {
		.name = "fsl-fman-memac-mdio",
		.of_match_table = memac_mdio_match,
	},
	.probe = memac_mdio_probe,
	.remove = memac_mdio_remove,
};

int __init memac_mdio_init(void)
{
	return platform_driver_register(&memac_mdio_driver);
}

void memac_mdio_exit(void)
{
	platform_driver_unregister(&memac_mdio_driver);
}
subsys_initcall_sync(memac_mdio_init);
module_exit(memac_mdio_exit);
