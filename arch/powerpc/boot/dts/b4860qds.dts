/*
 * B4860DS Device Tree Source
 *
 * Copyright 2012 Freescale Semiconductor Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Freescale Semiconductor nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *
 * ALTERNATIVELY, this software may be distributed under the terms of the
 * GNU General Public License ("GPL") as published by the Free Software
 * Foundation, either version 2 of that License or (at your option) any
 * later version.
 *
 * THIS SOFTWARE IS PROVIDED BY Freescale Semiconductor ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Freescale Semiconductor BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/include/ "fsl/b4860si-pre.dtsi"

/ {
	model = "fsl,B4860QDS";
	compatible = "fsl,B4860QDS";
	#address-cells = <2>;
	#size-cells = <2>;
	interrupt-parent = <&mpic>;

	aliases{
		ethernet0 = &enet0;
		ethernet1 = &enet1;
		ethernet2 = &enet2;
		ethernet3 = &enet3;
		ethernet4 = &enet4;
		ethernet5 = &enet5;
	};

	ifc: localbus@ffe124000 {
		reg = <0xf 0xfe124000 0 0x2000>;
		ranges = <0 0 0xf 0xe8000000 0x08000000
			  2 0 0xf 0xff800000 0x00010000
			  3 0 0xf 0xffdf0000 0x00008000>;

		nor@0,0 {
			#address-cells = <1>;
			#size-cells = <1>;
			compatible = "cfi-flash";
			reg = <0x0 0x0 0x8000000>;
			bank-width = <2>;
			device-width = <1>;
		};

		nand@2,0 {
			#address-cells = <1>;
			#size-cells = <1>;
			compatible = "fsl,ifc-nand";
			reg = <0x2 0x0 0x10000>;

			partition@0 {
				/* This location must not be altered  */
				/* 1MB for u-boot Bootloader Image */
				reg = <0x0 0x00100000>;
				label = "NAND U-Boot Image";
				read-only;
			};

			partition@100000 {
				/* 1MB for DTB Image */
				reg = <0x00100000 0x00100000>;
				label = "NAND DTB Image";
			};

			partition@200000 {
				/* 10MB for Linux Kernel Image */
				reg = <0x00200000 0x00A00000>;
				label = "NAND Linux Kernel Image";
			};

			partition@c00000 {
				/* 500MB for Root file System Image */
				reg = <0x00c00000 0x1F400000>;
				label = "NAND RFS Image";
			};
		};

		board-control@3,0 {
			compatible = "fsl,b4860qds-fpga", "fsl,fpga-qixis";
			reg = <3 0 0x300>;
		};
	};

	memory {
		device_type = "memory";
	};

	bportals: bman-portals@ff4000000 {
		ranges = <0x0 0xf 0xf4000000 0x2000000>;
	};

	qportals: qman-portals@ff6000000 {
		ranges = <0x0 0xf 0xf6000000 0x2000000>;
	};

	soc: soc@ffe000000 {
		ranges = <0x00000000 0xf 0xfe000000 0x1000000>;
		reg = <0xf 0xfe000000 0 0x00001000>;
		spi@110000 {
			flash@0 {
				#address-cells = <1>;
				#size-cells = <1>;
				compatible = "sst,sst25wf040";
				reg = <0>;
				spi-max-frequency = <40000000>; /* input clock */
			};
		};

		sdhc@114000 {
			status = "disabled";
		};

		i2c@118000 {
			eeprom@50 {
				compatible = "at24,24c64";
				reg = <0x50>;
			};
			eeprom@51 {
				compatible = "at24,24c256";
				reg = <0x51>;
			};
			eeprom@53 {
				compatible = "at24,24c256";
				reg = <0x53>;
			};
			eeprom@57 {
				compatible = "at24,24c256";
				reg = <0x57>;
			};
			rtc@68 {
				compatible = "dallas,ds3232";
				reg = <0x68>;
				interrupts = <0x1 0x1 0 0>;
			};
		};

		usb@210000 {
			dr_mode = "host";
			phy_type = "ulpi";
		};

		fman0: fman@400000 {
			enet0: ethernet@e0000 {
				fixed-link = <1 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet1: ethernet@e2000 {
				fixed-link = <2 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet2: ethernet@e4000 {
				fixed-link = <3 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet3: ethernet@e6000 {
				fixed-link = <4 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet4: ethernet@e8000 {
				fixed-link = <5 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet5: ethernet@ea000 {
				fixed-link = <6 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
		};
	};

	pci0: pcie@ffe200000 {
		reg = <0xf 0xfe200000 0 0x10000>;
		ranges = <0x02000000 0 0xe0000000 0xc 0x00000000 0x0 0x20000000
			  0x01000000 0 0x00000000 0xf 0xf8000000 0x0 0x00010000>;
		pcie@0 {
			ranges = <0x02000000 0 0xe0000000
				  0x02000000 0 0xe0000000
				  0 0x20000000

				  0x01000000 0 0x00000000
				  0x01000000 0 0x00000000
				  0 0x00010000>;
		};
	};

	rio: rapidio@ffe0c0000 {
		reg = <0xf 0xfe0c0000 0 0x11000>;

		port1 {
			ranges = <0 0 0xc 0x20000000 0 0x10000000>;
		};
		port2 {
			ranges = <0 0 0xc 0x30000000 0 0x10000000>;
		};
	};

	fsl,dpaa {
		compatible = "fsl,b4860-dpaa", "fsl,dpaa";
		ethernet@0 {
			compatible = "fsl,b4860-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet0>;
		};
		ethernet@1 {
			compatible = "fsl,b4860-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet1>;
		};
		ethernet@2 {
			compatible = "fsl,b4860-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet2>;
		};
		ethernet@3 {
			compatible = "fsl,b4860-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet3>;
		};
		ethernet@4 {
			compatible = "fsl,b4860-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet4>;
		};
		ethernet@5 {
			compatible = "fsl,b4860-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet5>;
		};
	};
};

/include/ "fsl/b4860si-post.dtsi"
/include/ "fsl/qoriq-dpaa-res3.dtsi"
