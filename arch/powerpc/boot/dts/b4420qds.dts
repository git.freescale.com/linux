/*
 * B4420DS Device Tree Source
 *
 * Copyright 2012 Freescale Semiconductor, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Freescale Semiconductor nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *
 * ALTERNATIVELY, this software may be distributed under the terms of the
 * GNU General Public License ("GPL") as published by the Free Software
 * Foundation, either version 2 of that License or (at your option) any
 * later version.
 *
 * This software is provided by Freescale Semiconductor "as is" and any
 * express or implied warranties, including, but not limited to, the implied
 * warranties of merchantability and fitness for a particular purpose are
 * disclaimed. In no event shall Freescale Semiconductor be liable for any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or services;
 * loss of use, data, or profits; or business interruption) however caused and
 * on any theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 */

/include/ "fsl/b4420si-pre.dtsi"

/ {
	model = "fsl,B4420QDS";
	compatible = "fsl,B4420QDS";
	#address-cells = <2>;
	#size-cells = <2>;
	interrupt-parent = <&mpic>;

	aliases{
		ethernet0 = &enet0;
		ethernet1 = &enet1;
		ethernet2 = &enet2;
		ethernet3 = &enet3;
	};

	ifc: localbus@ffe124000 {
		reg = <0xf 0xfe124000 0 0x2000>;
		ranges = <0 0 0xf 0xe8000000 0x08000000
			  2 0 0xf 0xff800000 0x00010000
			  3 0 0xf 0xffdf0000 0x00008000>;

		nor@0,0 {
			#address-cells = <1>;
			#size-cells = <1>;
			compatible = "cfi-flash";
			reg = <0x0 0x0 0x8000000>;
			bank-width = <2>;
			device-width = <1>;
		};

		nand@2,0 {
			#address-cells = <1>;
			#size-cells = <1>;
			compatible = "fsl,ifc-nand";
			reg = <0x2 0x0 0x10000>;

			partition@0 {
				/* This location must not be altered  */
				/* 1MB for u-boot Bootloader Image */
				reg = <0x0 0x00100000>;
				label = "NAND U-Boot Image";
				read-only;
			};

			partition@100000 {
				/* 1MB for DTB Image */
				reg = <0x00100000 0x00100000>;
				label = "NAND DTB Image";
			};

			partition@200000 {
				/* 10MB for Linux Kernel Image */
				reg = <0x00200000 0x00A00000>;
				label = "NAND Linux Kernel Image";
			};

			partition@c00000 {
				/* 500MB for Root file System Image */
				reg = <0x00c00000 0x1F400000>;
				label = "NAND RFS Image";
			};
		};

		board-control@3,0 {
			compatible = "fsl,b4420qds-fpga", "fsl,fpga-qixis";
			reg = <3 0 0x300>;
		};
	};

	memory {
		device_type = "memory";
	};

	bportals: bman-portals@ff4000000 {
		ranges = <0x0 0xf 0xf4000000 0x2000000>;
	};

	qportals: qman-portals@ff6000000 {
		ranges = <0x0 0xf 0xf6000000 0x2000000>;
	};

	soc: soc@ffe000000 {
		ranges = <0x00000000 0xf 0xfe000000 0x1000000>;
		reg = <0xf 0xfe000000 0 0x00001000>;
		spi@110000 {
			flash@0 {
				#address-cells = <1>;
				#size-cells = <1>;
				compatible = "sst,sst25wf040";
				reg = <0>;
				spi-max-frequency = <40000000>; /* input clock */
			};
		};

		sdhc@114000 {
			/*Disabled as there is no sdhc connector on B4420QDS board*/
			status = "disabled";
		};

		i2c@118000 {
			eeprom@50 {
				compatible = "at24,24c64";
				reg = <0x50>;
			};
			eeprom@51 {
				compatible = "at24,24c256";
				reg = <0x51>;
			};
			eeprom@53 {
				compatible = "at24,24c256";
				reg = <0x53>;
			};
			eeprom@57 {
				compatible = "at24,24c256";
				reg = <0x57>;
			};
			rtc@68 {
				compatible = "dallas,ds3232";
				reg = <0x68>;
				interrupts = <0x1 0x1 0 0>;
			};
		};

		usb@210000 {
			dr_mode = "host";
			phy_type = "ulpi";
		};

		fman0: fman@400000 {
			enet0: ethernet@e0000 {
				fixed-link = <1 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet1: ethernet@e2000 {
				fixed-link = <2 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet2: ethernet@e4000 {
				fixed-link = <3 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
			enet3: ethernet@e6000 {
				fixed-link = <4 1 1000 0 0 >;
				phy-connection-type = "sgmii";
			};
		};
	};

	pci0: pcie@ffe200000 {
		reg = <0xf 0xfe200000 0 0x10000>;
		ranges = <0x02000000 0 0xe0000000 0xc 0x00000000 0x0 0x20000000
			  0x01000000 0 0x00000000 0xf 0xf8000000 0x0 0x00010000>;
		pcie@0 {
			ranges = <0x02000000 0 0xe0000000
				  0x02000000 0 0xe0000000
				  0 0x20000000

				  0x01000000 0 0x00000000
				  0x01000000 0 0x00000000
				  0 0x00010000>;
		};
	};

	fsl,dpaa {
		compatible = "fsl,b4420-dpaa", "fsl,dpaa";
		ethernet@0 {
			compatible = "fsl,b4420-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet0>;
		};
		ethernet@1 {
			compatible = "fsl,b4420-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet1>;
		};
		ethernet@2 {
			compatible = "fsl,b4420-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet2>;
		};
		ethernet@3 {
			compatible = "fsl,b4420-dpa-ethernet", "fsl,dpa-ethernet";
			fsl,fman-mac = <&enet3>;
		};
	};
};

/include/ "fsl/b4420si-post.dtsi"
/include/ "fsl/qoriq-dpaa-res3.dtsi"
